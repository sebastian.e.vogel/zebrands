import React, { useState } from 'react'
import { render, fireEvent } from '../test/test-utils'

import { Pagination } from '../components/Pagination'

describe('Pagination test', () => {
  function WrapperPagination() {
    const [value, setValue] = useState(1)

    return (
      <Pagination
        totalData={31}
        currentPage={value}
        onHandlePaginate={(newValue) => {
          setValue(newValue)
        }}
      />
    )
  }

  const setup = () => {
    const wrapper = render(<WrapperPagination />)

    const input = wrapper.getByDisplayValue(1)
    const nextButton = wrapper.getByRole('button', { name: /next-page/i })
    const previousButton = wrapper.getByRole('button', {
      name: /previous-page/i,
    })
    return {
      input,
      nextButton,
      previousButton,
      wrapper,
    }
  }

  test('renders Pagination', () => {
    const { input, wrapper } = setup()
    wrapper.getByText('de 2')

    expect(input).toHaveValue(1)
  })

  test('change input text', () => {
    const { input } = setup()
    expect(input).toHaveValue(1)
    fireEvent.change(input, { target: { value: 2 } })
    expect(input).toHaveValue(2)
    fireEvent.change(input, { target: { value: 1 } })
    expect(input).toHaveValue(1)
  })

  test('clicking buttons', () => {
    const { input, nextButton, previousButton } = setup()
    expect(input).toHaveValue(1)

    fireEvent.click(nextButton)
    expect(input).toHaveValue(2)
    //max pages is 2 then input's value should be 2
    fireEvent.click(nextButton)
    expect(input).toHaveValue(2)
    fireEvent.click(previousButton)
    expect(input).toHaveValue(1)

    //min pages is 1 then input's value should be 1
    fireEvent.click(previousButton)
    expect(input).toHaveValue(1)
  })
})
