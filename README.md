# Zebrands test

## Summary

Create a Next.js application that allows users to search for github's users and repositories.
Created by Vogel Sebastian for Zebrand's front end test. 

## Setup

Make sure you have npm installed and write the follow commands in console:

```
git clone https://gitlab.com/sebastian.e.vogel/zebrands
cd zebrands/
yarn && yarn dev
or with npm:
npm install && npm run dev
```

Open http://localhost:3000 with your browser to see the result. Or see the result directly in this [DEMO](https://zebrands-sebastianevogel.vercel.app) 


### Enjoy your searches
