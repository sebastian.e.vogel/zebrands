import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../styles'
import '@testing-library/jest-dom'
import 'mutationobserver-shim'

const ThemeRenderer = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>
}

const customRender = (ui, options) =>
  render(ui, {
    wrapper: ThemeRenderer,
    ...options,
  })

export * from '@testing-library/react'
export { customRender as render }
