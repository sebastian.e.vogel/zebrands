import styled from 'styled-components'

export const Main = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1;
  padding: 0 3rem;
  @media (min-width: 768px) {
    padding: 0 10rem;
  }
`

export const Footer = styled.footer`
  padding: 1rem;
  background-color: ${({ theme }) => theme.colors.grey2};
  color: ${({ theme }) => theme.colors.primary};
`
