import { User } from '../../../types'
import { NoData } from '../../NoData'
import { Content, Container, Header, Avatar } from './components'

interface Props {
  users?: User[]
}

export const UserCard: React.FC<Props> = ({ users }) => {
  return (
    <>
      {users?.length ? (
        users.map(({ id, avatar_url, login, html_url }) => {
          return (
            <Container key={id}>
              <Header />
              <Avatar url={avatar_url} />
              <Content userName={login} githubUrl={html_url} />
            </Container>
          )
        })
      ) : (
        <NoData />
      )}
    </>
  )
}
