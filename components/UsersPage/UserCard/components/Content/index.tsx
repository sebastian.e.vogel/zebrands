import styled from 'styled-components'

const Container = styled.div`
  padding: 1rem;
  padding-top: 3rem;
`
const Name = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1.25rem;
  margin-bottom: 0.25rem;
  color: ${({ theme }) => theme.colors.primary};
`

const Link = styled.a`
  display: flex;
  justify-content: center;
  font-size: 0.75rem;
  color: ${({ theme }) => theme.colors.olive};
`

type Props = {
  userName: string
  githubUrl: string
}

export const Content: React.FC<Props> = ({ userName, githubUrl }) => (
  <Container>
    <Name>{userName}</Name>
    <Link href={githubUrl}>ir al perfil</Link>
  </Container>
)
