import styled from 'styled-components'

export { Content } from './Content'

export const Header = styled.div`
  position: relative;
  display: flex;
  flex: 0 0 auto;
  min-height: 6rem;
  height: 50%;
  background-color: ${({ theme }) => theme.colors.darkBlue};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  border-radius: 0.25rem 0.25rem 0 0;
`

export const Container = styled.div`
  position: relative;
  width: 250px;
  max-height: 300px;
  display: flex;
  flex-flow: column;
  background-color: ${({ theme }) => theme.colors.brown};
  border-radius: 0.25rem;
  margin: 1rem;
`

interface AvatarProps {
  url: string
}

export const Avatar = styled.div<AvatarProps>`
  position: absolute;
  display: flex;
  height: 6rem;
  width: 6rem;
  top: 3rem;
  left: calc(50% - 3rem);
  border: 4px solid ${({ theme }) => theme.colors.grey4};
  background-image: ${({ url }) => `url(${url})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  border-radius: 100%;
  z-index: 10;
`
