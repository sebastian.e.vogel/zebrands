import styled from 'styled-components'

export const NavbarContainer = styled.nav`
  width: 100%;
  max-width: 1000px;
  background-color: ${({ theme }) => theme.colors.grey2};
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  display: flex;
  flex-direction: column;
  height: 100px;
  padding: 2% 5%;
`

export const LeftContainer = styled.div`
  flex: 70%;
  display: flex;
  align-items: center;
`

export const RightContainer = styled.div`
  flex: 30%;
  display: flex;
  justify-content: flex-end;
`

export const NavbarInnerContainer = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

export const NavbarLinkContainer = styled.div`
  display: flex;
`

export const NavbarLink = styled.a`
  color: ${({ theme }) => theme.colors.primary};
  text-decoration: none;
  margin: 10px;
  cursor: pointer;
`

export const Logo = styled.img`
  margin: 10px;
  max-width: 180px;
`
