import React from 'react'
import {
  NavbarContainer,
  LeftContainer,
  RightContainer,
  NavbarInnerContainer,
  NavbarLinkContainer,
  NavbarLink,
} from './components'

import Image from 'next/image'
import Link from 'next/link'

export const Navbar = () => {
  return (
    <NavbarContainer>
      <NavbarInnerContainer>
        <LeftContainer>
          <Image src={'/logo.svg'} width={80} height={35} />
        </LeftContainer>
        <RightContainer>
          <NavbarLinkContainer>
            <Link href="/">
              <NavbarLink>Home</NavbarLink>
            </Link>
            <Link href="/users">
              <NavbarLink>Usuarios</NavbarLink>
            </Link>
            <Link href="/repositories">
              <NavbarLink>Repositorios</NavbarLink>
            </Link>
          </NavbarLinkContainer>
        </RightContainer>
      </NavbarInnerContainer>
    </NavbarContainer>
  )
}
