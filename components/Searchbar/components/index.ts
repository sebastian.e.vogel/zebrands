import styled from 'styled-components'

export const Container = styled.div`
  width: 80%;
  height: 40px;
  display: flex;
  align-items: center;
  padding: 0.4rem 0.9rem;
  border-radius: 6px;
  background-color: ${({ theme }) => theme.colors.grey2};
`
export const Input = styled.input`
  width: 100%;
  height: 100%;
  outline: none;
  border: none;
  font-weight: 500;
  background-color: transparent;
`

export const SearchIconContainer = styled.div`
  font-size: 27px;
  margin-right: 10px;
  margin-top: 6px;
  vertical-align: middle;
`
