import { Input, Container, SearchIconContainer } from './components'

import Image from 'next/image'
import { useDebouncedCallback } from 'use-debounce'

interface Props {
  onChange: (textToSearch: string) => void
}

export const Searchbar: React.FC<Props> = ({ onChange }) => {
  const debounced = useDebouncedCallback((text: string) => {
    onChange(text)
  }, 800)

  return (
    <Container>
      <SearchIconContainer>
        <Image width={20} height={25} src={'/search-icon.svg'} />
      </SearchIconContainer>
      <Input placeholder="Buscar" onChange={(e) => debounced(e.target.value)} />
    </Container>
  )
}
