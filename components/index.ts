import styled from 'styled-components'
export { Navbar } from './NavBar'
export { Searchbar } from './Searchbar'
export { Pagination } from './Pagination'
export { Loader } from './Loader'
export { NoData } from './NoData'

export const Layout = styled.div`
  padding: 0 2rem;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.grey1};
`

export const Container = styled.div`
  max-width: 1000px;
  flex: 1;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const PagesContainer = styled.div`
  max-width: 1000px;
  width: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 1rem 0;
`

export const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  flex: 1;
  width: 100%;
`
