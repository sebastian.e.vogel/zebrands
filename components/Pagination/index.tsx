import Image from 'next/image'
import React, { useEffect } from 'react'

import { Container, Button, Input } from './components'

interface Props {
  totalData?: number
  currentPage: number
  onHandlePaginate: (page: number) => void
}

export const Pagination: React.FC<Props> = ({
  totalData,
  currentPage = 1,
  onHandlePaginate,
}) => {
  const totalPages = totalData ? Math.ceil(totalData / 30) : 1
  const isLeftButtonDisabled = currentPage === 1
  const isRightButtonDisabled = currentPage === totalPages

  useEffect(() => {
    if (currentPage > totalPages) onHandlePaginate(1)
  }, [totalPages])

  return (
    <Container>
      <Input
        value={currentPage}
        type="number"
        max={totalPages}
        min={1}
        onChange={(e) => {
          const newPage = e.target.value as unknown as number
          onHandlePaginate(newPage)
        }}
      />
      <span> de {totalPages}</span>
      <div>
        <Button
          aria-label="previous-page"
          onClick={() => onHandlePaginate(currentPage - 1)}
          disabled={isLeftButtonDisabled}
        >
          <Image src={'/arrow-left.svg'} width={10} height={16} />
        </Button>
        <Button
          aria-label="next-page"
          onClick={() => onHandlePaginate(currentPage + 1)}
          disabled={isRightButtonDisabled}
        >
          <Image src={'/arrow-right.svg'} width={10} height={16} />
        </Button>
      </div>
    </Container>
  )
}
