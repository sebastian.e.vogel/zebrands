import styled from 'styled-components'

export const Input = styled.input`
  width: 50px;
  height: 100%;
  outline: none;
  border: none;
  font-weight: 500;
  font-size: 1rem;
  background-color: transparent;
  text-align: center;
  border: 1px solid rgb(207, 217, 224);
  padding: 5px;
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-width: 150px;
  max-width: 200px;
`

export const Button = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 5px;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  background-color: ${({ theme }) => theme.colors.grey3};
`
