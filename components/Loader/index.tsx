import { Balls, Text, Container } from './components'

export const Loader = () => {
  return (
    <Container>
      <Text>Cargando</Text>
      <Balls>
        <span className="ball one"></span>
        <span className="ball two"></span>
        <span className="ball three"></span>
      </Balls>
    </Container>
  )
}
