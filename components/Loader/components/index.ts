import styled from 'styled-components'

export const Balls = styled.div`
  display: flex;
  justify-content: center;

  .ball {
    height: 20px;
    width: 20px;
    border-radius: 50%;
    background-color: ${({ theme }) => theme.colors.darkPrimary};
    margin: 0 6px 0 0;
    animation: oscillate 0.7s ease-in forwards infinite;
  }

  .one {
    animation-delay: 0.5s;
  }
  .two {
    animation-delay: 1s;
  }
  .three {
    animation-delay: 2s;
  }

  @keyframes oscillate {
    0% {
      transform: translateY(0);
    }
    50% {
      transform: translateY(20px);
    }
    100% {
      transform: translateY(0);
    }
  }
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`
export const Text = styled.span`
  margin-bottom: 15px;
  font-size: 1.5rem;
  color: ${({ theme }) => theme.colors.primary};
`
