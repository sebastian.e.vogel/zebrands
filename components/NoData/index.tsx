import styled from 'styled-components'

export const Text = styled.span`
  margin-bottom: 15px;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.primary};
`

export const Container = styled.div`
  display: flex;
  align-items: center;
`

export const NoData = () => {
  return (
    <Container>
      <Text>No se han encontrado resultados, intenta una nueva búsqueda</Text>
    </Container>
  )
}
