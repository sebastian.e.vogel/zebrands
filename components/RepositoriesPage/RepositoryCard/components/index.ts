import styled from 'styled-components'

export const Container = styled.div`
  position: relative;
  width: 250px;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.colors.lightPrimary};
  border-radius: 0.25rem;
  margin: 1rem;
  padding: 1rem;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`

export const Link = styled.a`
  font-size: 0.75rem;
  color: ${({ theme }) => theme.colors.olive};
`

export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.primary};
  overflow: hidden;
  text-overflow: ellipsis;
`

export const OwnerName = styled.span`
  color: ${({ theme }) => theme.colors.olive};
  font-size: 0.75rem;
`

export const Divider = styled.div`
  width: 2px;
  background: ${({ theme }) => theme.colors.primary};
  height: 100%;
  margin: 0 5px;
`

export const Description = styled.p`
  font-size: 0.8rem;
`
export const Footer = styled.div`
  display: flex;
`
