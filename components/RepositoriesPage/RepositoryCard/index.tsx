import React from 'react'
import { Repository } from '../../../types'
import { NoData } from '../../NoData'
import {
  Container,
  Link,
  Title,
  OwnerName,
  Divider,
  Description,
  Footer,
} from './components'

interface Props {
  repositories?: Repository[]
}

export const RepositoryCard: React.FC<Props> = ({ repositories }) => (
  <>
    {repositories?.length ? (
      repositories
        ?.slice(0, 30)
        .map(({ id, name, owner, html_url, description }) => {
          return (
            <Container key={id}>
              <Title>{name}</Title>
              <Description>{description}</Description>
              <Footer>
                <OwnerName>Creado por {owner.login}</OwnerName>
                <Divider />
                <Link href={html_url}>Ir al repositorio</Link>
              </Footer>
            </Container>
          )
        })
    ) : (
      <NoData />
    )}
  </>
)
