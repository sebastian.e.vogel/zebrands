import { useState, useEffect } from 'react'

export function usePagination() {
  const [currentPage, setCurrentPage] = useState(1)

  return { currentPage, setCurrentPage }
}
