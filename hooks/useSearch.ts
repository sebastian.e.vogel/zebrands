import { useState, useEffect } from 'react'

export function useSearch<T>(url?: string) {
  const [data, setData] = useState<T>()
  const [loading, setLoading] = useState(false)
  const [totalDataNumber, setTotalDataNumber] = useState(0)

  useEffect(() => {
    if (!url) {
      setData(undefined)
      setTotalDataNumber(0)
      return
    }
    setLoading(true)
    const getData = async () => {
      const apiResponse = await fetch(url)
      const data = await apiResponse.json()
      setData(data.items)
      setTotalDataNumber(data.total_count)
      setLoading(false)
    }
    getData()
  }, [url])

  return { data, loading, totalDataNumber }
}
