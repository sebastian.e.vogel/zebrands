export const theme = {
  colors: {
    grey1: '#f2f3f4',
    grey2: '#dcdcdc',
    grey3: '#e3e9ed',
    grey4: '#ececec',
    primary: '#2c3e50',
    lightPrimary: '#ecf0f1',
    darkPrimary: '#1b5299',
    olive: '#7f8c8d',
    brown: '#ebd8c3',
    darkBlue: '#0e293e',
  },
}
