import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Layout, Navbar } from '../components'
import { ThemeProvider } from 'styled-components'
import { theme } from '../styles'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <Navbar />
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  )
}

export default MyApp
